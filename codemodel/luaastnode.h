/*
 * Copyright (c) 2016
 *	Jarosław Pelczar <jarek@jpelczar.com>.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef LUAPLUGIN_INTERNAL_AST_ASTNODE_H
#define LUAPLUGIN_INTERNAL_AST_ASTNODE_H

#include "luaastfwd.h"
#include "luaastmanaged.h"
#include "luaplugin_global.h"

namespace LuaPlugin {
namespace CodeModel {

class LUAPLUGINSHARED_EXPORT ASTNode : public Managed
{
protected:
    ASTNode(ASTType type);

public:
    virtual ~ASTNode();

    inline ASTType type() const { return _type; }

    virtual SourceLocation firstSourceLocation() const = 0;
    virtual SourceLocation lastSourceLocation() const = 0;
    virtual void accept0(ASTVisitor * visitor) = 0;

    virtual ASTBlock * asBlock() { return nullptr; }
    virtual ASTIdentifier * asIdentifier() { return nullptr; }
    virtual ASTStatement * asStatement() { return nullptr; }
    virtual ASTExpression * asExpression() { return nullptr; }

    void accept(ASTVisitor *visitor);
    static void accept(ASTNode *node, ASTVisitor *visitor);

private:
    ASTType _type;
};

template<typename _Node> class ASTList : public Managed
{
    _Node *             _node;
    ASTList<_Node> *    _next;
public:
    _Node * node() const {
        return _node;
    }

    _Node * last() const {

    }
};

class LUAPLUGINSHARED_EXPORT ASTBlock final : public ASTNode
{
public:

private:

};

} // namespace CodeModel
} // namespace LuaPlugin

#endif // LUAPLUGIN_INTERNAL_AST_ASTNODE_H
