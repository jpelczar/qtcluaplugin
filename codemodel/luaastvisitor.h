#ifndef LUAPLUGIN_CODEMODEL_ASTVISITOR_H
#define LUAPLUGIN_CODEMODEL_ASTVISITOR_H

#include "luaastfwd.h"

namespace LuaPlugin {
namespace CodeModel {

class LUAPLUGINSHARED_EXPORT ASTVisitor
{
public:
    ASTVisitor();
    virtual ~ASTVisitor();

    virtual bool preVisit(ASTNode * node) { Q_UNUSED(node); return true; }
    virtual void postVisit(ASTNode * node) { Q_UNUSED(node); }

    virtual bool visit(ASTBlock * node) { Q_UNUSED(node); return true; }
    virtual void endVisit(ASTBlock * node) { Q_UNUSED(node); }

    virtual bool visit(ASTIdentifier * node) { Q_UNUSED(node); return true; }
    virtual void endVisit(ASTIdentifier * node) { Q_UNUSED(node); }
};

} // namespace CodeModel
} // namespace LuaPlugin

#endif // LUAPLUGIN_CODEMODEL_ASTVISITOR_H
