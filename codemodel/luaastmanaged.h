/*
 * Copyright (c) 2016
 *	Jarosław Pelczar <jarek@jpelczar.com>.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef LUAPLUGIN_CODEMODEL_MANAGED_H
#define LUAPLUGIN_CODEMODEL_MANAGED_H

#include <QtGlobal>
#include "luaplugin_global.h"

namespace LuaPlugin {
namespace CodeModel {

class LUAPLUGINSHARED_EXPORT MemoryPool final
{
public:
    MemoryPool(size_t block = 8192);
    ~MemoryPool();

    MemoryPool(const MemoryPool&) = delete;
    MemoryPool& operator = (const MemoryPool&) = delete;

    void * allocate(size_t size);

private:
    struct _DataBlock;
    _DataBlock * _data;
};

class LUAPLUGINSHARED_EXPORT Managed
{
public:
    Managed() = default;
    ~Managed() = default;

    Managed(const Managed&) = delete;
    Managed& operator = (const Managed&) = delete;

    void * operator new(size_t size, MemoryPool * pool) { return pool->allocate(size); }
    void operator delete(void *) { }
    void operator delete(void *, MemoryPool *) { }
};

} // namespace CodeModel
} // namespace LuaPlugin

#endif // LUAPLUGIN_CODEMODEL_MANAGED_H
