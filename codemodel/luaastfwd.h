/*
 * Copyright (c) 2016
 *	Jarosław Pelczar <jarek@jpelczar.com>.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef LUAPLUGIN_INTERNAL_AST_LUAASTFWD_H
#define LUAPLUGIN_INTERNAL_AST_LUAASTFWD_H

#include "luaplugin_global.h"

namespace LuaPlugin {
namespace CodeModel {

class ASTNode;
class ASTVisitor;
class ASTBlock;
class ASTIdentifier;

class ASTStatement;
class ASTExpression;

template<typename _Node> class ASTList;

enum class ASTType {
    Undefined,
    Block,
    Identifier
};

struct LUAPLUGINSHARED_EXPORT SourceLocation
{
    int offset;
    int length;
    int line;
    int column;

    inline SourceLocation(int _offset = 0, int _length = 0, int _line = 0, int _column = 0) :
        offset(_offset),
        length(_length),
        line(_line),
        column(_column)
    {
    }

    inline bool isNull() const
    {
        return length == 0;
    }

    inline bool isValid() const
    {
        return length > 0;
    }
};

} // namespace CodeModel
} // namespace LuaPlugin

#endif // LUAPLUGIN_INTERNAL_AST_LUAASTFWD_H
