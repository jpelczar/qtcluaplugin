/*
 * Copyright (c) 2016
 *	Jarosław Pelczar <jarek@jpelczar.com>.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "luaastmanaged.h"

namespace LuaPlugin {
namespace CodeModel {

struct MemoryPool::_DataBlock
{
    char *          _buffer;
    size_t          _size;
    size_t          _used;
    _DataBlock *    _next;

    _DataBlock(size_t size) :
        _buffer(new char[size]),
        _size(size),
        _used(0),
        _next(nullptr)
    {
    }

    ~_DataBlock()
    {
        delete[] _buffer;
    }
};

MemoryPool::MemoryPool(size_t size)
{
    _data = new _DataBlock(size);
}

MemoryPool::~MemoryPool()
{
    _DataBlock * cur, * next;

    cur = _data;

    while(cur) {
        next = cur->_next;
        delete cur;
        cur = next;
    }
}

void * MemoryPool::allocate(size_t size)
{
    size = (size + sizeof(void *) - 1) & ~(sizeof(void *) - 1);

    if((size + _data->_used) > _data->_size) {
        _DataBlock * block = new _DataBlock(_data->_size);
        block->_next = _data;
        _data = block;
    }

    void * result = _data->_buffer + _data->_used;
    _data->_used += size;
    return result;
}

} // namespace CodeModel
} // namespace LuaPlugin
