DEFINES += LUAPLUGIN_LIBRARY

# LuaPlugin files

INCLUDEPATH += lua-5.3.0/src $$PWD

SOURCES += luapluginplugin.cpp \
    codemodel/luaastnode.cpp \
    lua-5.3.0.cpp \
    codemodel/luaastmanaged.cpp \
    codemodel/luaastvisitor.cpp

HEADERS += luapluginplugin.h \
        luaplugin_global.h \
        luapluginconstants.h \
    codemodel/luaastnode.h \
    codemodel/luaastfwd.h \
    lua-5.3.0.hpp \
    codemodel/luaastmanaged.h \
    codemodel/luaastvisitor.h

# Qt Creator linking

## set the QTC_SOURCE environment variable to override the setting here
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=/home/jarek/Projects/qt-creator

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=/home/jarek/Projects/build-qtcreator-Desktop_Qt_5_6_0_GCC_64bit-Debug

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
# USE_USER_DESTDIR = yes

###### If the plugin can be depended upon by other plugins, this code needs to be outsourced to
###### <dirname>_dependencies.pri, where <dirname> is the name of the directory containing the
###### plugin's sources.

QTC_PLUGIN_NAME = LuaPlugin
QTC_LIB_DEPENDS += \
    extensionsystem \
    utils

QTC_PLUGIN_DEPENDS += \
    coreplugin \
    texteditor

QTC_PLUGIN_RECOMMENDS += \
    # optional plugin dependencies. nothing here at this time

###### End _dependencies.pri contents ######

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)

DISTFILES += \
    LuaPlugin.mimetypes.xml

RESOURCES += \
    luaplugin.qrc
